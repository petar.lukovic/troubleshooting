﻿
namespace troubleshooting
{
    partial class window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title8 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title9 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(window));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title10 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title11 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title12 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title13 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title14 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.panel1 = new System.Windows.Forms.Panel();
            this.display_error = new System.Windows.Forms.TextBox();
            this.warining_light = new System.Windows.Forms.Label();
            this.LV_curr_value_bar = new System.Windows.Forms.Label();
            this.water_pres_value_bar = new System.Windows.Forms.Label();
            this.water_temp_value_bar = new System.Windows.Forms.Label();
            this.engine_speed_value_bar = new System.Windows.Forms.Label();
            this.HV_bat_temp_value_bar = new System.Windows.Forms.Label();
            this.HV_curr_value_bar = new System.Windows.Forms.Label();
            this.LV_curr_value = new System.Windows.Forms.Label();
            this.water_pres_value = new System.Windows.Forms.Label();
            this.water_temp_value = new System.Windows.Forms.Label();
            this.engine_speed_value = new System.Windows.Forms.Label();
            this.HV_bat_temp_value = new System.Windows.Forms.Label();
            this.HV_curr_value = new System.Windows.Forms.Label();
            this.LV_bat_value = new System.Windows.Forms.Label();
            this.LV_bat_value_bar = new System.Windows.Forms.Label();
            this.LV_curr_bar = new System.Windows.Forms.Label();
            this.HV_temp_bar = new System.Windows.Forms.Label();
            this.HV_curr_bar = new System.Windows.Forms.Label();
            this.engine_speed_bar = new System.Windows.Forms.Label();
            this.water_temp_bar = new System.Windows.Forms.Label();
            this.LV_voltage_bar = new System.Windows.Forms.Label();
            this.display_speed = new System.Windows.Forms.TextBox();
            this.water_press_bar = new System.Windows.Forms.Label();
            this.BayesianNetwork = new System.Windows.Forms.Panel();
            this.IMD_f = new System.Windows.Forms.Label();
            this.invertor_f = new System.Windows.Forms.Label();
            this.engine_f = new System.Windows.Forms.Label();
            this.battery_low_f = new System.Windows.Forms.Label();
            this.pump_f = new System.Windows.Forms.Label();
            this.cooling_system_f = new System.Windows.Forms.Label();
            this.ventilator_f = new System.Windows.Forms.Label();
            this.overheat_f = new System.Windows.Forms.Label();
            this.shut_down_f = new System.Windows.Forms.Label();
            this.power_loss_f = new System.Windows.Forms.Label();
            this.BMS_f = new System.Windows.Forms.Label();
            this.HV_curr_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.LV_voltage_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Start = new System.Windows.Forms.Button();
            this.BN_graph = new System.Windows.Forms.Label();
            this.HV_curr_button = new System.Windows.Forms.Button();
            this.engine_speed_button = new System.Windows.Forms.Button();
            this.HV_temp_button = new System.Windows.Forms.Button();
            this.water_temp_button = new System.Windows.Forms.Button();
            this.water_press_button = new System.Windows.Forms.Button();
            this.LV_curr_button = new System.Windows.Forms.Button();
            this.LV_voltage_button = new System.Windows.Forms.Button();
            this.BN_button = new System.Windows.Forms.Button();
            this.LV_voltage_graph = new System.Windows.Forms.Label();
            this.HV_curr_graph = new System.Windows.Forms.Label();
            this.engine_speed_graph = new System.Windows.Forms.Label();
            this.HV_temp_graph = new System.Windows.Forms.Label();
            this.water_temp_graph = new System.Windows.Forms.Label();
            this.water_press_graph = new System.Windows.Forms.Label();
            this.LV_curr_graph = new System.Windows.Forms.Label();
            this.engine_speed_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.HV_temp_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.water_temp_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.LV_curr_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.water_press_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.BayesianNetwork.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HV_curr_chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LV_voltage_chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.engine_speed_chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HV_temp_chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.water_temp_chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LV_curr_chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.water_press_chart)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.Controls.Add(this.display_error);
            this.panel1.Controls.Add(this.warining_light);
            this.panel1.Controls.Add(this.LV_curr_value_bar);
            this.panel1.Controls.Add(this.water_pres_value_bar);
            this.panel1.Controls.Add(this.water_temp_value_bar);
            this.panel1.Controls.Add(this.engine_speed_value_bar);
            this.panel1.Controls.Add(this.HV_bat_temp_value_bar);
            this.panel1.Controls.Add(this.HV_curr_value_bar);
            this.panel1.Controls.Add(this.LV_curr_value);
            this.panel1.Controls.Add(this.water_pres_value);
            this.panel1.Controls.Add(this.water_temp_value);
            this.panel1.Controls.Add(this.engine_speed_value);
            this.panel1.Controls.Add(this.HV_bat_temp_value);
            this.panel1.Controls.Add(this.HV_curr_value);
            this.panel1.Controls.Add(this.LV_bat_value);
            this.panel1.Controls.Add(this.LV_bat_value_bar);
            this.panel1.Controls.Add(this.LV_curr_bar);
            this.panel1.Controls.Add(this.HV_temp_bar);
            this.panel1.Controls.Add(this.HV_curr_bar);
            this.panel1.Controls.Add(this.engine_speed_bar);
            this.panel1.Controls.Add(this.water_temp_bar);
            this.panel1.Controls.Add(this.LV_voltage_bar);
            this.panel1.Controls.Add(this.display_speed);
            this.panel1.Controls.Add(this.water_press_bar);
            this.panel1.Location = new System.Drawing.Point(258, 523);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1221, 266);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // display_error
            // 
            this.display_error.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.display_error.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.display_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.display_error.ForeColor = System.Drawing.Color.Lime;
            this.display_error.Location = new System.Drawing.Point(456, 29);
            this.display_error.Multiline = true;
            this.display_error.Name = "display_error";
            this.display_error.Size = new System.Drawing.Size(287, 50);
            this.display_error.TabIndex = 3;
            this.display_error.Text = "NO ERROR.\r\n";
            this.display_error.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // warining_light
            // 
            this.warining_light.AutoSize = true;
            this.warining_light.BackColor = System.Drawing.Color.Red;
            this.warining_light.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.warining_light.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warining_light.Location = new System.Drawing.Point(760, 29);
            this.warining_light.Name = "warining_light";
            this.warining_light.Size = new System.Drawing.Size(32, 27);
            this.warining_light.TabIndex = 0;
            this.warining_light.Text = "   ";
            // 
            // LV_curr_value_bar
            // 
            this.LV_curr_value_bar.AutoSize = true;
            this.LV_curr_value_bar.BackColor = System.Drawing.Color.Lime;
            this.LV_curr_value_bar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LV_curr_value_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_curr_value_bar.Location = new System.Drawing.Point(855, 208);
            this.LV_curr_value_bar.Name = "LV_curr_value_bar";
            this.LV_curr_value_bar.Size = new System.Drawing.Size(301, 22);
            this.LV_curr_value_bar.TabIndex = 37;
            this.LV_curr_value_bar.Text = "                                                          ";
            // 
            // water_pres_value_bar
            // 
            this.water_pres_value_bar.AutoSize = true;
            this.water_pres_value_bar.BackColor = System.Drawing.Color.Lime;
            this.water_pres_value_bar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.water_pres_value_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_pres_value_bar.Location = new System.Drawing.Point(855, 150);
            this.water_pres_value_bar.Name = "water_pres_value_bar";
            this.water_pres_value_bar.Size = new System.Drawing.Size(301, 22);
            this.water_pres_value_bar.TabIndex = 36;
            this.water_pres_value_bar.Text = "                                                          ";
            this.water_pres_value_bar.Click += new System.EventHandler(this.label13_Click);
            // 
            // water_temp_value_bar
            // 
            this.water_temp_value_bar.AutoSize = true;
            this.water_temp_value_bar.BackColor = System.Drawing.Color.Lime;
            this.water_temp_value_bar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.water_temp_value_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_temp_value_bar.Location = new System.Drawing.Point(855, 93);
            this.water_temp_value_bar.Name = "water_temp_value_bar";
            this.water_temp_value_bar.Size = new System.Drawing.Size(301, 22);
            this.water_temp_value_bar.TabIndex = 35;
            this.water_temp_value_bar.Text = "                                                          ";
            // 
            // engine_speed_value_bar
            // 
            this.engine_speed_value_bar.AutoSize = true;
            this.engine_speed_value_bar.BackColor = System.Drawing.Color.Lime;
            this.engine_speed_value_bar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.engine_speed_value_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.engine_speed_value_bar.Location = new System.Drawing.Point(81, 208);
            this.engine_speed_value_bar.Name = "engine_speed_value_bar";
            this.engine_speed_value_bar.Size = new System.Drawing.Size(301, 22);
            this.engine_speed_value_bar.TabIndex = 34;
            this.engine_speed_value_bar.Text = "                                                          ";
            // 
            // HV_bat_temp_value_bar
            // 
            this.HV_bat_temp_value_bar.AutoSize = true;
            this.HV_bat_temp_value_bar.BackColor = System.Drawing.Color.Lime;
            this.HV_bat_temp_value_bar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HV_bat_temp_value_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_bat_temp_value_bar.Location = new System.Drawing.Point(81, 150);
            this.HV_bat_temp_value_bar.Name = "HV_bat_temp_value_bar";
            this.HV_bat_temp_value_bar.Size = new System.Drawing.Size(301, 22);
            this.HV_bat_temp_value_bar.TabIndex = 33;
            this.HV_bat_temp_value_bar.Text = "                                                          ";
            this.HV_bat_temp_value_bar.Click += new System.EventHandler(this.label10_Click);
            // 
            // HV_curr_value_bar
            // 
            this.HV_curr_value_bar.AutoSize = true;
            this.HV_curr_value_bar.BackColor = System.Drawing.Color.Lime;
            this.HV_curr_value_bar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HV_curr_value_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_curr_value_bar.Location = new System.Drawing.Point(81, 93);
            this.HV_curr_value_bar.Name = "HV_curr_value_bar";
            this.HV_curr_value_bar.Size = new System.Drawing.Size(301, 22);
            this.HV_curr_value_bar.TabIndex = 32;
            this.HV_curr_value_bar.Text = "                                                          ";
            // 
            // LV_curr_value
            // 
            this.LV_curr_value.AutoSize = true;
            this.LV_curr_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_curr_value.Location = new System.Drawing.Point(797, 210);
            this.LV_curr_value.Name = "LV_curr_value";
            this.LV_curr_value.Size = new System.Drawing.Size(67, 20);
            this.LV_curr_value.TabIndex = 31;
            this.LV_curr_value.Text = "label2    ";
            // 
            // water_pres_value
            // 
            this.water_pres_value.AutoSize = true;
            this.water_pres_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_pres_value.Location = new System.Drawing.Point(797, 152);
            this.water_pres_value.Name = "water_pres_value";
            this.water_pres_value.Size = new System.Drawing.Size(67, 20);
            this.water_pres_value.TabIndex = 30;
            this.water_pres_value.Text = "label2    ";
            // 
            // water_temp_value
            // 
            this.water_temp_value.AutoSize = true;
            this.water_temp_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_temp_value.Location = new System.Drawing.Point(797, 93);
            this.water_temp_value.Name = "water_temp_value";
            this.water_temp_value.Size = new System.Drawing.Size(67, 20);
            this.water_temp_value.TabIndex = 29;
            this.water_temp_value.Text = "label2    ";
            // 
            // engine_speed_value
            // 
            this.engine_speed_value.AutoSize = true;
            this.engine_speed_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.engine_speed_value.Location = new System.Drawing.Point(17, 208);
            this.engine_speed_value.Name = "engine_speed_value";
            this.engine_speed_value.Size = new System.Drawing.Size(67, 20);
            this.engine_speed_value.TabIndex = 28;
            this.engine_speed_value.Text = "label2    ";
            // 
            // HV_bat_temp_value
            // 
            this.HV_bat_temp_value.AutoSize = true;
            this.HV_bat_temp_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_bat_temp_value.Location = new System.Drawing.Point(17, 150);
            this.HV_bat_temp_value.Name = "HV_bat_temp_value";
            this.HV_bat_temp_value.Size = new System.Drawing.Size(67, 20);
            this.HV_bat_temp_value.TabIndex = 27;
            this.HV_bat_temp_value.Text = "label2    ";
            // 
            // HV_curr_value
            // 
            this.HV_curr_value.AutoSize = true;
            this.HV_curr_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_curr_value.Location = new System.Drawing.Point(17, 93);
            this.HV_curr_value.Name = "HV_curr_value";
            this.HV_curr_value.Size = new System.Drawing.Size(67, 20);
            this.HV_curr_value.TabIndex = 26;
            this.HV_curr_value.Text = "label2    ";
            // 
            // LV_bat_value
            // 
            this.LV_bat_value.AutoSize = true;
            this.LV_bat_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_bat_value.Location = new System.Drawing.Point(17, 44);
            this.LV_bat_value.Name = "LV_bat_value";
            this.LV_bat_value.Size = new System.Drawing.Size(67, 20);
            this.LV_bat_value.TabIndex = 0;
            this.LV_bat_value.Text = "label2    ";
            // 
            // LV_bat_value_bar
            // 
            this.LV_bat_value_bar.AutoSize = true;
            this.LV_bat_value_bar.BackColor = System.Drawing.Color.Lime;
            this.LV_bat_value_bar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LV_bat_value_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_bat_value_bar.Location = new System.Drawing.Point(81, 44);
            this.LV_bat_value_bar.Name = "LV_bat_value_bar";
            this.LV_bat_value_bar.Size = new System.Drawing.Size(301, 22);
            this.LV_bat_value_bar.TabIndex = 25;
            this.LV_bat_value_bar.Text = "                                                          ";
            // 
            // LV_curr_bar
            // 
            this.LV_curr_bar.AutoSize = true;
            this.LV_curr_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_curr_bar.Location = new System.Drawing.Point(797, 190);
            this.LV_curr_bar.Name = "LV_curr_bar";
            this.LV_curr_bar.Size = new System.Drawing.Size(162, 20);
            this.LV_curr_bar.TabIndex = 22;
            this.LV_curr_bar.Text = "LV_current_sensor";
            // 
            // HV_temp_bar
            // 
            this.HV_temp_bar.AutoSize = true;
            this.HV_temp_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_temp_bar.Location = new System.Drawing.Point(17, 130);
            this.HV_temp_bar.Name = "HV_temp_bar";
            this.HV_temp_bar.Size = new System.Drawing.Size(272, 20);
            this.HV_temp_bar.TabIndex = 19;
            this.HV_temp_bar.Text = "HV_battery_temperature_sensor";
            // 
            // HV_curr_bar
            // 
            this.HV_curr_bar.AutoSize = true;
            this.HV_curr_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_curr_bar.Location = new System.Drawing.Point(17, 73);
            this.HV_curr_bar.Name = "HV_curr_bar";
            this.HV_curr_bar.Size = new System.Drawing.Size(165, 20);
            this.HV_curr_bar.TabIndex = 17;
            this.HV_curr_bar.Text = "HV_current_sensor";
            this.HV_curr_bar.Click += new System.EventHandler(this.label9_Click);
            // 
            // engine_speed_bar
            // 
            this.engine_speed_bar.AutoSize = true;
            this.engine_speed_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.engine_speed_bar.Location = new System.Drawing.Point(17, 188);
            this.engine_speed_bar.Name = "engine_speed_bar";
            this.engine_speed_bar.Size = new System.Drawing.Size(242, 20);
            this.engine_speed_bar.TabIndex = 18;
            this.engine_speed_bar.Text = "engine_rotary_speed_sensor";
            // 
            // water_temp_bar
            // 
            this.water_temp_bar.AutoSize = true;
            this.water_temp_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_temp_bar.Location = new System.Drawing.Point(797, 73);
            this.water_temp_bar.Name = "water_temp_bar";
            this.water_temp_bar.Size = new System.Drawing.Size(359, 20);
            this.water_temp_bar.TabIndex = 20;
            this.water_temp_bar.Text = "water_temperature_sensor_invertor_engine";
            // 
            // LV_voltage_bar
            // 
            this.LV_voltage_bar.AutoSize = true;
            this.LV_voltage_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_voltage_bar.Location = new System.Drawing.Point(17, 24);
            this.LV_voltage_bar.Name = "LV_voltage_bar";
            this.LV_voltage_bar.Size = new System.Drawing.Size(229, 20);
            this.LV_voltage_bar.TabIndex = 11;
            this.LV_voltage_bar.Text = "LV_battery_voltage_sensor";
            this.LV_voltage_bar.Click += new System.EventHandler(this.label8_Click);
            // 
            // display_speed
            // 
            this.display_speed.BackColor = System.Drawing.Color.Black;
            this.display_speed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.display_speed.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.display_speed.ForeColor = System.Drawing.Color.Lime;
            this.display_speed.Location = new System.Drawing.Point(456, 65);
            this.display_speed.Multiline = true;
            this.display_speed.Name = "display_speed";
            this.display_speed.ReadOnly = true;
            this.display_speed.Size = new System.Drawing.Size(287, 172);
            this.display_speed.TabIndex = 2;
            this.display_speed.Text = "----------------\r\n148 km/h";
            this.display_speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // water_press_bar
            // 
            this.water_press_bar.AutoSize = true;
            this.water_press_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_press_bar.Location = new System.Drawing.Point(797, 130);
            this.water_press_bar.Name = "water_press_bar";
            this.water_press_bar.Size = new System.Drawing.Size(197, 20);
            this.water_press_bar.TabIndex = 21;
            this.water_press_bar.Text = "water_pressure_sensor";
            // 
            // BayesianNetwork
            // 
            this.BayesianNetwork.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BayesianNetwork.BackgroundImage = global::troubleshooting.Properties.Resources.bajesove;
            this.BayesianNetwork.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BayesianNetwork.Controls.Add(this.IMD_f);
            this.BayesianNetwork.Controls.Add(this.invertor_f);
            this.BayesianNetwork.Controls.Add(this.engine_f);
            this.BayesianNetwork.Controls.Add(this.battery_low_f);
            this.BayesianNetwork.Controls.Add(this.pump_f);
            this.BayesianNetwork.Controls.Add(this.cooling_system_f);
            this.BayesianNetwork.Controls.Add(this.ventilator_f);
            this.BayesianNetwork.Controls.Add(this.overheat_f);
            this.BayesianNetwork.Controls.Add(this.shut_down_f);
            this.BayesianNetwork.Controls.Add(this.power_loss_f);
            this.BayesianNetwork.Controls.Add(this.BMS_f);
            this.BayesianNetwork.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BayesianNetwork.Location = new System.Drawing.Point(258, 0);
            this.BayesianNetwork.Name = "BayesianNetwork";
            this.BayesianNetwork.Size = new System.Drawing.Size(1221, 530);
            this.BayesianNetwork.TabIndex = 1;
            this.BayesianNetwork.Paint += new System.Windows.Forms.PaintEventHandler(this.BayesianNetwork_Paint);
            // 
            // IMD_f
            // 
            this.IMD_f.AutoSize = true;
            this.IMD_f.BackColor = System.Drawing.Color.Lime;
            this.IMD_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IMD_f.Location = new System.Drawing.Point(291, 150);
            this.IMD_f.Name = "IMD_f";
            this.IMD_f.Size = new System.Drawing.Size(120, 18);
            this.IMD_f.TabIndex = 20;
            this.IMD_f.Text = "99.99% normal";
            // 
            // invertor_f
            // 
            this.invertor_f.AutoSize = true;
            this.invertor_f.BackColor = System.Drawing.Color.Lime;
            this.invertor_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.invertor_f.Location = new System.Drawing.Point(552, 107);
            this.invertor_f.Name = "invertor_f";
            this.invertor_f.Size = new System.Drawing.Size(120, 18);
            this.invertor_f.TabIndex = 19;
            this.invertor_f.Text = "99.99% normal";
            // 
            // engine_f
            // 
            this.engine_f.AutoSize = true;
            this.engine_f.BackColor = System.Drawing.Color.Lime;
            this.engine_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.engine_f.Location = new System.Drawing.Point(429, 170);
            this.engine_f.Name = "engine_f";
            this.engine_f.Size = new System.Drawing.Size(120, 18);
            this.engine_f.TabIndex = 18;
            this.engine_f.Text = "99.99% normal";
            // 
            // battery_low_f
            // 
            this.battery_low_f.AutoSize = true;
            this.battery_low_f.BackColor = System.Drawing.Color.Lime;
            this.battery_low_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.battery_low_f.Location = new System.Drawing.Point(652, 192);
            this.battery_low_f.Name = "battery_low_f";
            this.battery_low_f.Size = new System.Drawing.Size(120, 18);
            this.battery_low_f.TabIndex = 17;
            this.battery_low_f.Text = "99.99% normal";
            this.battery_low_f.Click += new System.EventHandler(this.baterry_low_Click);
            // 
            // pump_f
            // 
            this.pump_f.AutoSize = true;
            this.pump_f.BackColor = System.Drawing.Color.Lime;
            this.pump_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pump_f.Location = new System.Drawing.Point(785, 152);
            this.pump_f.Name = "pump_f";
            this.pump_f.Size = new System.Drawing.Size(120, 18);
            this.pump_f.TabIndex = 16;
            this.pump_f.Text = "99.99% normal";
            // 
            // cooling_system_f
            // 
            this.cooling_system_f.AutoSize = true;
            this.cooling_system_f.BackColor = System.Drawing.Color.Lime;
            this.cooling_system_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cooling_system_f.Location = new System.Drawing.Point(940, 182);
            this.cooling_system_f.Name = "cooling_system_f";
            this.cooling_system_f.Size = new System.Drawing.Size(120, 18);
            this.cooling_system_f.TabIndex = 15;
            this.cooling_system_f.Text = "99.99% normal";
            // 
            // ventilator_f
            // 
            this.ventilator_f.AutoSize = true;
            this.ventilator_f.BackColor = System.Drawing.Color.Lime;
            this.ventilator_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ventilator_f.Location = new System.Drawing.Point(989, 265);
            this.ventilator_f.Name = "ventilator_f";
            this.ventilator_f.Size = new System.Drawing.Size(120, 18);
            this.ventilator_f.TabIndex = 14;
            this.ventilator_f.Text = "99.99% normal";
            // 
            // overheat_f
            // 
            this.overheat_f.AutoSize = true;
            this.overheat_f.BackColor = System.Drawing.Color.Lime;
            this.overheat_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.overheat_f.Location = new System.Drawing.Point(817, 265);
            this.overheat_f.Name = "overheat_f";
            this.overheat_f.Size = new System.Drawing.Size(120, 18);
            this.overheat_f.TabIndex = 13;
            this.overheat_f.Text = "99.99% normal";
            this.overheat_f.Click += new System.EventHandler(this.label3_Click_1);
            // 
            // shut_down_f
            // 
            this.shut_down_f.AutoSize = true;
            this.shut_down_f.BackColor = System.Drawing.Color.Lime;
            this.shut_down_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.shut_down_f.Location = new System.Drawing.Point(274, 258);
            this.shut_down_f.Name = "shut_down_f";
            this.shut_down_f.Size = new System.Drawing.Size(120, 18);
            this.shut_down_f.TabIndex = 12;
            this.shut_down_f.Text = "99.99% normal";
            // 
            // power_loss_f
            // 
            this.power_loss_f.AutoSize = true;
            this.power_loss_f.BackColor = System.Drawing.Color.Lime;
            this.power_loss_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.power_loss_f.Location = new System.Drawing.Point(534, 256);
            this.power_loss_f.Name = "power_loss_f";
            this.power_loss_f.Size = new System.Drawing.Size(120, 18);
            this.power_loss_f.TabIndex = 11;
            this.power_loss_f.Text = "99.99% normal";
            // 
            // BMS_f
            // 
            this.BMS_f.AutoSize = true;
            this.BMS_f.BackColor = System.Drawing.Color.Lime;
            this.BMS_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BMS_f.Location = new System.Drawing.Point(139, 170);
            this.BMS_f.Name = "BMS_f";
            this.BMS_f.Size = new System.Drawing.Size(120, 18);
            this.BMS_f.TabIndex = 0;
            this.BMS_f.Text = "99.99% normal";
            // 
            // HV_curr_chart
            // 
            this.HV_curr_chart.BackColor = System.Drawing.Color.Orange;
            chartArea8.AxisX.Interval = 3D;
            chartArea8.AxisY.Title = "current [A]";
            chartArea8.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            chartArea8.Name = "ChartArea1";
            this.HV_curr_chart.ChartAreas.Add(chartArea8);
            this.HV_curr_chart.ImeMode = System.Windows.Forms.ImeMode.On;
            this.HV_curr_chart.Location = new System.Drawing.Point(258, 0);
            this.HV_curr_chart.Name = "HV_curr_chart";
            series8.BorderWidth = 3;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Legend = "Legend1";
            series8.Name = "Series1";
            this.HV_curr_chart.Series.Add(series8);
            this.HV_curr_chart.Size = new System.Drawing.Size(1221, 530);
            this.HV_curr_chart.TabIndex = 0;
            this.HV_curr_chart.Text = "chart1";
            title8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title8.Name = "Title1";
            title8.Text = "HV_current_sensor_reading";
            this.HV_curr_chart.Titles.Add(title8);
            this.HV_curr_chart.Click += new System.EventHandler(this.HV_curr_chart_Click);
            // 
            // LV_voltage_chart
            // 
            this.LV_voltage_chart.BackColor = System.Drawing.Color.Orange;
            chartArea9.AxisX.Interval = 3D;
            chartArea9.AxisY.Maximum = 22D;
            chartArea9.AxisY.Minimum = 8D;
            chartArea9.AxisY.Title = "voltage [V]";
            chartArea9.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            chartArea9.Name = "ChartArea1";
            this.LV_voltage_chart.ChartAreas.Add(chartArea9);
            this.LV_voltage_chart.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.LV_voltage_chart.Location = new System.Drawing.Point(258, 0);
            this.LV_voltage_chart.Name = "LV_voltage_chart";
            this.LV_voltage_chart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            series9.BorderWidth = 3;
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Legend = "Legend1";
            series9.Name = "bla";
            series9.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series9.YValuesPerPoint = 2;
            series9.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.LV_voltage_chart.Series.Add(series9);
            this.LV_voltage_chart.Size = new System.Drawing.Size(1221, 530);
            this.LV_voltage_chart.TabIndex = 0;
            this.LV_voltage_chart.Text = "chart1";
            title9.BackColor = System.Drawing.Color.Transparent;
            title9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title9.Name = "Title1";
            title9.Text = "LV_battery_voltage_sensor_reading";
            this.LV_voltage_chart.Titles.Add(title9);
            this.LV_voltage_chart.Click += new System.EventHandler(this.LV_voltage_chart_Click);
            // 
            // Start
            // 
            this.Start.BackColor = System.Drawing.Color.Lime;
            this.Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Start.Location = new System.Drawing.Point(49, 594);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(150, 137);
            this.Start.TabIndex = 0;
            this.Start.Text = "START";
            this.Start.UseVisualStyleBackColor = false;
            this.Start.Click += new System.EventHandler(this.button1_Click);
            // 
            // BN_graph
            // 
            this.BN_graph.AutoSize = true;
            this.BN_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BN_graph.Location = new System.Drawing.Point(12, 42);
            this.BN_graph.Name = "BN_graph";
            this.BN_graph.Size = new System.Drawing.Size(152, 20);
            this.BN_graph.TabIndex = 0;
            this.BN_graph.Text = "Bayesian Network";
            this.BN_graph.Click += new System.EventHandler(this.BN_Click);
            // 
            // HV_curr_button
            // 
            this.HV_curr_button.BackgroundImage = global::troubleshooting.Properties.Resources.WhatsApp_Image_2021_12_31_at_16_05_00;
            this.HV_curr_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HV_curr_button.Location = new System.Drawing.Point(180, 152);
            this.HV_curr_button.Name = "HV_curr_button";
            this.HV_curr_button.Size = new System.Drawing.Size(62, 58);
            this.HV_curr_button.TabIndex = 4;
            this.HV_curr_button.UseVisualStyleBackColor = true;
            this.HV_curr_button.Click += new System.EventHandler(this.HV_curr_button_Click);
            // 
            // engine_speed_button
            // 
            this.engine_speed_button.BackgroundImage = global::troubleshooting.Properties.Resources.WhatsApp_Image_2021_12_31_at_16_05_00;
            this.engine_speed_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.engine_speed_button.Location = new System.Drawing.Point(180, 216);
            this.engine_speed_button.Name = "engine_speed_button";
            this.engine_speed_button.Size = new System.Drawing.Size(62, 58);
            this.engine_speed_button.TabIndex = 5;
            this.engine_speed_button.UseVisualStyleBackColor = true;
            this.engine_speed_button.Click += new System.EventHandler(this.engine_speed_button_Click);
            // 
            // HV_temp_button
            // 
            this.HV_temp_button.BackgroundImage = global::troubleshooting.Properties.Resources.WhatsApp_Image_2021_12_31_at_16_05_00;
            this.HV_temp_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HV_temp_button.Location = new System.Drawing.Point(180, 280);
            this.HV_temp_button.Name = "HV_temp_button";
            this.HV_temp_button.Size = new System.Drawing.Size(62, 58);
            this.HV_temp_button.TabIndex = 6;
            this.HV_temp_button.UseVisualStyleBackColor = true;
            this.HV_temp_button.Click += new System.EventHandler(this.button5_Click);
            // 
            // water_temp_button
            // 
            this.water_temp_button.BackgroundImage = global::troubleshooting.Properties.Resources.WhatsApp_Image_2021_12_31_at_16_05_00;
            this.water_temp_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.water_temp_button.Location = new System.Drawing.Point(180, 344);
            this.water_temp_button.Name = "water_temp_button";
            this.water_temp_button.Size = new System.Drawing.Size(62, 58);
            this.water_temp_button.TabIndex = 7;
            this.water_temp_button.UseVisualStyleBackColor = true;
            this.water_temp_button.Click += new System.EventHandler(this.water_temp_button_Click);
            // 
            // water_press_button
            // 
            this.water_press_button.BackgroundImage = global::troubleshooting.Properties.Resources.WhatsApp_Image_2021_12_31_at_16_05_00;
            this.water_press_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.water_press_button.Location = new System.Drawing.Point(180, 408);
            this.water_press_button.Name = "water_press_button";
            this.water_press_button.Size = new System.Drawing.Size(62, 58);
            this.water_press_button.TabIndex = 8;
            this.water_press_button.UseVisualStyleBackColor = true;
            this.water_press_button.Click += new System.EventHandler(this.water_press_button_Click);
            // 
            // LV_curr_button
            // 
            this.LV_curr_button.BackgroundImage = global::troubleshooting.Properties.Resources.WhatsApp_Image_2021_12_31_at_16_05_00;
            this.LV_curr_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LV_curr_button.Location = new System.Drawing.Point(180, 472);
            this.LV_curr_button.Name = "LV_curr_button";
            this.LV_curr_button.Size = new System.Drawing.Size(62, 58);
            this.LV_curr_button.TabIndex = 9;
            this.LV_curr_button.UseVisualStyleBackColor = true;
            this.LV_curr_button.Click += new System.EventHandler(this.LV_curr_button_Click);
            // 
            // LV_voltage_button
            // 
            this.LV_voltage_button.BackgroundImage = global::troubleshooting.Properties.Resources.WhatsApp_Image_2021_12_31_at_16_05_00;
            this.LV_voltage_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LV_voltage_button.Location = new System.Drawing.Point(180, 88);
            this.LV_voltage_button.Name = "LV_voltage_button";
            this.LV_voltage_button.Size = new System.Drawing.Size(62, 58);
            this.LV_voltage_button.TabIndex = 3;
            this.LV_voltage_button.UseVisualStyleBackColor = true;
            this.LV_voltage_button.Click += new System.EventHandler(this.button2_Click);
            // 
            // BN_button
            // 
            this.BN_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BN_button.BackgroundImage")));
            this.BN_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BN_button.Location = new System.Drawing.Point(180, 24);
            this.BN_button.Name = "BN_button";
            this.BN_button.Size = new System.Drawing.Size(62, 58);
            this.BN_button.TabIndex = 2;
            this.BN_button.UseVisualStyleBackColor = true;
            this.BN_button.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // LV_voltage_graph
            // 
            this.LV_voltage_graph.AutoSize = true;
            this.LV_voltage_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_voltage_graph.Location = new System.Drawing.Point(9, 97);
            this.LV_voltage_graph.Name = "LV_voltage_graph";
            this.LV_voltage_graph.Size = new System.Drawing.Size(165, 40);
            this.LV_voltage_graph.TabIndex = 10;
            this.LV_voltage_graph.Text = "LV_battery_voltage\r\n       _sensor";
            this.LV_voltage_graph.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // HV_curr_graph
            // 
            this.HV_curr_graph.AutoSize = true;
            this.HV_curr_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_curr_graph.Location = new System.Drawing.Point(12, 170);
            this.HV_curr_graph.Name = "HV_curr_graph";
            this.HV_curr_graph.Size = new System.Drawing.Size(165, 20);
            this.HV_curr_graph.TabIndex = 11;
            this.HV_curr_graph.Text = "HV_current_sensor";
            // 
            // engine_speed_graph
            // 
            this.engine_speed_graph.AutoSize = true;
            this.engine_speed_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.engine_speed_graph.Location = new System.Drawing.Point(24, 225);
            this.engine_speed_graph.Name = "engine_speed_graph";
            this.engine_speed_graph.Size = new System.Drawing.Size(132, 40);
            this.engine_speed_graph.TabIndex = 12;
            this.engine_speed_graph.Text = "engine_rotary\r\n_speed_sensor";
            this.engine_speed_graph.Click += new System.EventHandler(this.label3_Click);
            // 
            // HV_temp_graph
            // 
            this.HV_temp_graph.AutoSize = true;
            this.HV_temp_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HV_temp_graph.Location = new System.Drawing.Point(-4, 289);
            this.HV_temp_graph.Name = "HV_temp_graph";
            this.HV_temp_graph.Size = new System.Drawing.Size(181, 40);
            this.HV_temp_graph.TabIndex = 13;
            this.HV_temp_graph.Text = "         HV_battery\r\n_temperature_sensor";
            this.HV_temp_graph.Click += new System.EventHandler(this.label4_Click);
            // 
            // water_temp_graph
            // 
            this.water_temp_graph.AutoSize = true;
            this.water_temp_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_temp_graph.Location = new System.Drawing.Point(12, 344);
            this.water_temp_graph.Name = "water_temp_graph";
            this.water_temp_graph.Size = new System.Drawing.Size(166, 60);
            this.water_temp_graph.TabIndex = 14;
            this.water_temp_graph.Text = " water_temperature\r\n        _sensor\r\n  _invertor_engine";
            // 
            // water_press_graph
            // 
            this.water_press_graph.AutoSize = true;
            this.water_press_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.water_press_graph.Location = new System.Drawing.Point(24, 417);
            this.water_press_graph.Name = "water_press_graph";
            this.water_press_graph.Size = new System.Drawing.Size(133, 40);
            this.water_press_graph.TabIndex = 15;
            this.water_press_graph.Text = "water_pressure\r\n      _sensor";
            // 
            // LV_curr_graph
            // 
            this.LV_curr_graph.AutoSize = true;
            this.LV_curr_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_curr_graph.Location = new System.Drawing.Point(12, 490);
            this.LV_curr_graph.Name = "LV_curr_graph";
            this.LV_curr_graph.Size = new System.Drawing.Size(162, 20);
            this.LV_curr_graph.TabIndex = 16;
            this.LV_curr_graph.Text = "LV_current_sensor";
            // 
            // engine_speed_chart
            // 
            this.engine_speed_chart.BackColor = System.Drawing.Color.Orange;
            chartArea10.AxisX.Interval = 3D;
            chartArea10.AxisY.Title = "w [krpm]";
            chartArea10.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            chartArea10.Name = "ChartArea1";
            this.engine_speed_chart.ChartAreas.Add(chartArea10);
            this.engine_speed_chart.Location = new System.Drawing.Point(258, 0);
            this.engine_speed_chart.Name = "engine_speed_chart";
            series10.BorderWidth = 3;
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Legend = "Legend1";
            series10.Name = "Series1";
            this.engine_speed_chart.Series.Add(series10);
            this.engine_speed_chart.Size = new System.Drawing.Size(1221, 530);
            this.engine_speed_chart.TabIndex = 0;
            this.engine_speed_chart.Text = "chart1";
            title10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title10.Name = "Title1";
            title10.Text = "engine_rotary_speed_sensor_reading";
            this.engine_speed_chart.Titles.Add(title10);
            this.engine_speed_chart.Click += new System.EventHandler(this.engine_speed_chart_Click);
            // 
            // HV_temp_chart
            // 
            this.HV_temp_chart.BackColor = System.Drawing.Color.Orange;
            chartArea11.AxisX.Interval = 3D;
            chartArea11.AxisY.Title = "temperature [Celsius]";
            chartArea11.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            chartArea11.Name = "ChartArea1";
            this.HV_temp_chart.ChartAreas.Add(chartArea11);
            this.HV_temp_chart.Location = new System.Drawing.Point(258, 0);
            this.HV_temp_chart.Name = "HV_temp_chart";
            series11.BorderWidth = 3;
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Legend = "Legend1";
            series11.Name = "Series1";
            this.HV_temp_chart.Series.Add(series11);
            this.HV_temp_chart.Size = new System.Drawing.Size(1221, 530);
            this.HV_temp_chart.TabIndex = 0;
            this.HV_temp_chart.Text = "chart1";
            title11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title11.Name = "Title1";
            title11.Text = "HV_battery_temperature_sensor_reading";
            this.HV_temp_chart.Titles.Add(title11);
            // 
            // water_temp_chart
            // 
            this.water_temp_chart.BackColor = System.Drawing.Color.Orange;
            chartArea12.AxisX.Interval = 3D;
            chartArea12.AxisY.Title = "temperature [Celsius]";
            chartArea12.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            chartArea12.BorderWidth = 3;
            chartArea12.Name = "ChartArea1";
            this.water_temp_chart.ChartAreas.Add(chartArea12);
            this.water_temp_chart.Location = new System.Drawing.Point(258, 0);
            this.water_temp_chart.Name = "water_temp_chart";
            series12.BorderWidth = 3;
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Legend = "Legend1";
            series12.Name = "Series1";
            this.water_temp_chart.Series.Add(series12);
            this.water_temp_chart.Size = new System.Drawing.Size(1221, 530);
            this.water_temp_chart.TabIndex = 0;
            this.water_temp_chart.Text = "chart1";
            title12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title12.Name = "Title1";
            title12.Text = "water_temperature_sensor_invertor_engine_reading";
            this.water_temp_chart.Titles.Add(title12);
            // 
            // LV_curr_chart
            // 
            this.LV_curr_chart.BackColor = System.Drawing.Color.Orange;
            chartArea13.AxisX.Interval = 3D;
            chartArea13.AxisY.Title = "current [A]";
            chartArea13.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            chartArea13.BorderWidth = 3;
            chartArea13.Name = "ChartArea1";
            this.LV_curr_chart.ChartAreas.Add(chartArea13);
            this.LV_curr_chart.Location = new System.Drawing.Point(258, 0);
            this.LV_curr_chart.Name = "LV_curr_chart";
            series13.BorderWidth = 3;
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Legend = "Legend1";
            series13.Name = "Series1";
            this.LV_curr_chart.Series.Add(series13);
            this.LV_curr_chart.Size = new System.Drawing.Size(1221, 530);
            this.LV_curr_chart.TabIndex = 2;
            this.LV_curr_chart.Text = "chart3";
            title13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title13.Name = "Title1";
            title13.Text = "LV_current_sensor_reading";
            this.LV_curr_chart.Titles.Add(title13);
            // 
            // water_press_chart
            // 
            this.water_press_chart.BackColor = System.Drawing.Color.Orange;
            chartArea14.AxisX.Interval = 3D;
            chartArea14.AxisY.Title = "pressure [kPa]";
            chartArea14.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            chartArea14.Name = "ChartArea1";
            this.water_press_chart.ChartAreas.Add(chartArea14);
            this.water_press_chart.Location = new System.Drawing.Point(258, 0);
            this.water_press_chart.Name = "water_press_chart";
            series14.BorderWidth = 3;
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Legend = "Legend1";
            series14.Name = "Series1";
            this.water_press_chart.Series.Add(series14);
            this.water_press_chart.Size = new System.Drawing.Size(1221, 530);
            this.water_press_chart.TabIndex = 3;
            this.water_press_chart.Text = "chart4";
            title14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title14.Name = "Title1";
            title14.Text = "water_pressure_sensor_reading";
            this.water_press_chart.Titles.Add(title14);
            // 
            // timer
            // 
            this.timer.Interval = 2000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CadetBlue;
            this.ClientSize = new System.Drawing.Size(1481, 788);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LV_curr_graph);
            this.Controls.Add(this.LV_voltage_graph);
            this.Controls.Add(this.water_press_graph);
            this.Controls.Add(this.water_temp_graph);
            this.Controls.Add(this.HV_temp_graph);
            this.Controls.Add(this.engine_speed_graph);
            this.Controls.Add(this.HV_curr_graph);
            this.Controls.Add(this.LV_curr_button);
            this.Controls.Add(this.water_press_button);
            this.Controls.Add(this.water_temp_button);
            this.Controls.Add(this.HV_temp_button);
            this.Controls.Add(this.engine_speed_button);
            this.Controls.Add(this.HV_curr_button);
            this.Controls.Add(this.LV_voltage_button);
            this.Controls.Add(this.BN_button);
            this.Controls.Add(this.BN_graph);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.BayesianNetwork);
            this.Controls.Add(this.LV_voltage_chart);
            this.Controls.Add(this.engine_speed_chart);
            this.Controls.Add(this.HV_curr_chart);
            this.Controls.Add(this.HV_temp_chart);
            this.Controls.Add(this.water_press_chart);
            this.Controls.Add(this.water_temp_chart);
            this.Controls.Add(this.LV_curr_chart);
            this.Name = "window";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Bashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.BayesianNetwork.ResumeLayout(false);
            this.BayesianNetwork.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HV_curr_chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LV_voltage_chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.engine_speed_chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HV_temp_chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.water_temp_chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LV_curr_chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.water_press_chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel BayesianNetwork;

        private System.Windows.Forms.Button Start;
      
        private System.Windows.Forms.Button BN_button;
        private System.Windows.Forms.Button LV_voltage_button;
        private System.Windows.Forms.Button HV_curr_button;
        private System.Windows.Forms.Button engine_speed_button;
        private System.Windows.Forms.Button HV_temp_button;
        private System.Windows.Forms.Button water_temp_button;
        private System.Windows.Forms.Button water_press_button;
        private System.Windows.Forms.Button LV_curr_button;

        private System.Windows.Forms.Label BN_graph;
        private System.Windows.Forms.Label LV_voltage_graph;
        private System.Windows.Forms.Label HV_curr_graph;
        private System.Windows.Forms.Label engine_speed_graph;
        private System.Windows.Forms.Label HV_temp_graph;
        private System.Windows.Forms.Label water_temp_graph;
        private System.Windows.Forms.Label water_press_graph;
        private System.Windows.Forms.Label LV_curr_graph;

        private System.Windows.Forms.TextBox display_error;
        private System.Windows.Forms.TextBox display_speed;

        private System.Windows.Forms.Label LV_voltage_bar;
        private System.Windows.Forms.Label HV_curr_bar;
        private System.Windows.Forms.Label engine_speed_bar;
        private System.Windows.Forms.Label HV_temp_bar;
        private System.Windows.Forms.Label water_temp_bar;
        private System.Windows.Forms.Label water_press_bar;
        private System.Windows.Forms.Label LV_curr_bar;

        private System.Windows.Forms.Label LV_curr_value_bar;
        private System.Windows.Forms.Label water_pres_value_bar;
        private System.Windows.Forms.Label water_temp_value_bar;
        private System.Windows.Forms.Label engine_speed_value_bar;
        private System.Windows.Forms.Label HV_bat_temp_value_bar;
        private System.Windows.Forms.Label HV_curr_value_bar;
        private System.Windows.Forms.Label LV_bat_value_bar;

        private System.Windows.Forms.Label LV_curr_value;
        private System.Windows.Forms.Label water_pres_value;
        private System.Windows.Forms.Label water_temp_value;
        private System.Windows.Forms.Label engine_speed_value;
        private System.Windows.Forms.Label HV_bat_temp_value;
        private System.Windows.Forms.Label HV_curr_value;
        private System.Windows.Forms.Label LV_bat_value;

        private System.Windows.Forms.Label warining_light;

        private System.Windows.Forms.DataVisualization.Charting.Chart LV_voltage_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart HV_curr_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart engine_speed_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart HV_temp_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart water_temp_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart LV_curr_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart water_press_chart;

        private System.Windows.Forms.Label IMD_f;
        private System.Windows.Forms.Label invertor_f;
        private System.Windows.Forms.Label engine_f;
        private System.Windows.Forms.Label battery_low_f;
        private System.Windows.Forms.Label pump_f;
        private System.Windows.Forms.Label cooling_system_f;
        private System.Windows.Forms.Label ventilator_f;
        private System.Windows.Forms.Label overheat_f;
        private System.Windows.Forms.Label shut_down_f;
        private System.Windows.Forms.Label power_loss_f;
        private System.Windows.Forms.Label BMS_f;
        private System.Windows.Forms.Timer timer;
    }
}

