﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Windows;

namespace troubleshooting
{
    public partial class window : Form
    {
		private StreamReader input_file;

		public window() {
			InitializeComponent();
            BayesianNetwork.Visible = true;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = false;

			LV_curr_value.Text = "0.0 A";
			water_pres_value.Text = "0.0 kpa";
			water_temp_value.Text = "0.0 C";
			engine_speed_value.Text = "0.0 krpm";
			HV_bat_temp_value.Text = "0.0 C";
			HV_curr_value.Text = "0.0 A";
			LV_bat_value.Text = "0.0 V";

			display_error.Text = "NO ERROR.";
			warining_light.BackColor = Color.Lime;

			input_file = new StreamReader("sensor_reading.txt");

			for(int i = 0; i < 30; i++)
            {
				LV_voltage_readings.Add(0);
				HV_curr_readings.Add(0);
				engine_rpm_readings.Add(0);
				HV_temp_readings.Add(0);
				water_temp_readings.Add(0);
				water_press_readings.Add(0);
				LV_curr_readings.Add(0);
			}
		}

		private int speed = 150;
		private void timer_Tick(object sender, EventArgs e)
		{
			base.OnActivated(e);
			Random rnd = new Random();
			if (input_file.Peek() >= 0)
			{
				double LV_voltage_r = 0;
				double LV_curr_r = 0;
				double HV_curr_r = 0;
				double HV_temp_r = 0;
				double engine_rpm__r = 0;
				double water_press_r = 0;
				double water_temp_r = 0;

				string line = input_file.ReadLine();
				string[] doubles = line.Split(' ');

				LV_voltage_r = Convert.ToDouble(doubles[0]);
				HV_curr_r = Convert.ToDouble(doubles[1]);
				engine_rpm__r = Convert.ToDouble(doubles[2]);
				HV_temp_r = Convert.ToDouble(doubles[3]);
				water_temp_r = Convert.ToDouble(doubles[4]);
				water_press_r = Convert.ToDouble(doubles[5]);
				LV_curr_r = Convert.ToDouble(doubles[6]);

				LV_voltage_refresh(LV_voltage_r);
				HV_curr_refresh(HV_curr_r);
				engine_rpm_refresh(engine_rpm__r);
				HV_temp_refresh(HV_temp_r);
				water_temp_refresh(water_temp_r);
				water_press_refresh(water_press_r);
				LV_curr_refresh(LV_curr_r);

				denumerator = calcDenumertator();

				BMS_f_refresh();
				IMD_f_refresh();
				engine_f_refresh();
				invertor_f_refresh();
				battery_low_refresh();
				pump_f_refresh();
				cooling_system_f_refresh();
				ventilator_f_refresh();
				overheat_refresh();
				power_loss_refresh();
				shut_down_f_refresh();

				checkErrors();

				speed = speed + (int)(rnd.NextDouble() * 3 - rnd.NextDouble() * 5);
				display_speed.Text = "----------------\n" + speed.ToString() + " km/h";
				display_speed.Update();
			}
			else
			{
				display_speed.Text = "----------------\n" + "0 km/h";
				display_error.Text = "SIMULATION IS OVER...";
				display_error.Update();
				display_speed.Update();
				Thread.Sleep(10000);
				this.Dispose();
            }
		}

		private double[] fault_rates = new double[11];
		private string[] error_names = new string[11];

		private void checkErrors()
        {
			int mini = 0;
			for(int i = 0; i < 8; i++)
            {
				if(fault_rates[mini] > fault_rates[i]) { mini = i; }
            }

			int minj = 0;
			for (int j = 8; j < 11; j++)
			{
				if (fault_rates[minj] > fault_rates[j]) { minj = j; }
			}

			if(fault_rates[mini] < 65)
            {
				warining_light.BackColor = Color.Red;
				display_error.Text = error_names[minj] + "\n" + error_names[mini];
			}
            else
            {
				warining_light.BackColor = Color.Lime;
				display_error.Text = "NO ERROR.";
			}
			display_error.Update();
			warining_light.Update();
		}

        private void BMS_f_refresh()
        {
			double p = calcBMSf();
			BMS_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) BMS_f.BackColor = Color.Red;
			else
			{
				if (p < 85) BMS_f.BackColor = Color.Yellow;
				else BMS_f.BackColor = Color.Lime;
			}
			BMS_f.Update();
			fault_rates[0] = p;
			error_names[0] = "CHECK BMS BOARD.";
		}

		private void IMD_f_refresh()
		{
			double p = calcIMDf();
			IMD_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) IMD_f.BackColor = Color.Red;
			else
			{
				if (p < 85) IMD_f.BackColor = Color.Yellow;
				else IMD_f.BackColor = Color.Lime;
			}
			IMD_f.Update();
			fault_rates[1] = p;
			error_names[1] = "CHECK IMD BOARD.";
		}

		private void engine_f_refresh()
		{
			double p = calcEnginef();
			engine_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) engine_f.BackColor = Color.Red;
			else
			{
				if (p < 85) engine_f.BackColor = Color.Yellow;
				else engine_f.BackColor = Color.Lime;
			}
			engine_f.Update();
			fault_rates[2] = p;
			error_names[2] = "CHECK ENGINE.";
		}

		private void invertor_f_refresh()
		{
			double p = calcInvertorf();
			invertor_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) invertor_f.BackColor = Color.Red;
			else
			{
				if (p < 85) invertor_f.BackColor = Color.Yellow;
				else invertor_f.BackColor = Color.Lime;
			}
			invertor_f.Update();
			fault_rates[3] = p;
			error_names[3] = "CHECK INVERTOR.";
		}

		private void battery_low_refresh()
		{
			double p = calcBatteryLowf();
			battery_low_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) battery_low_f.BackColor = Color.Red;
			else
			{
				if (p < 85) battery_low_f.BackColor = Color.Yellow;
				else battery_low_f.BackColor = Color.Lime;
			}
			battery_low_f.Update();
			fault_rates[4] = p;
			error_names[4] = "CHECK BATTERY LEVEL.";
		}

		private void pump_f_refresh()
		{
			double p = calcPumpf();
			pump_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) pump_f.BackColor = Color.Red;
			else
			{
				if (p < 85) pump_f.BackColor = Color.Yellow;
				else pump_f.BackColor = Color.Lime;
			}
			pump_f.Update();
			fault_rates[5] = p;
			error_names[5] = "CHECK PUMPS.";
		}

		private void cooling_system_f_refresh()
		{
			double p = calcCoolingSystemf();
			cooling_system_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) cooling_system_f.BackColor = Color.Red;
			else
			{
				if (p < 85) cooling_system_f.BackColor = Color.Yellow;
				else cooling_system_f.BackColor = Color.Lime;
			}
			cooling_system_f.Update();
			fault_rates[6] = p;
			error_names[6] = "CHECK SYSTEM LEAKS.";
		}

		private void ventilator_f_refresh()
		{
			double p = calcVentilatorf();
			ventilator_f.Text = p.ToString("N2") + "% normal";
			if (p < 65) ventilator_f.BackColor = Color.Red;
			else
			{
				if (p < 85) ventilator_f.BackColor = Color.Yellow;
				else ventilator_f.BackColor = Color.Lime;
			}
			ventilator_f.Update();
			fault_rates[7] = p;
			error_names[7] = "CHECK VENTILATORS.";
		}

		private void overheat_refresh()
		{
			double p = calcOverheat();
			overheat_f.Text = p.ToString("N2") + "% normal";
			if (p < 30) overheat_f.BackColor = Color.Red;
			else
			{
				if (p < 50) overheat_f.BackColor = Color.Yellow;
				else overheat_f.BackColor = Color.Lime;
			}
			overheat_f.Update();
			fault_rates[8] = p;
			error_names[8] = "CHECK for OVERHEATING.";
		}

		private void power_loss_refresh()
		{
			double p = calcPowerLoss();
			power_loss_f.Text = p.ToString("N2") + "% normal";
			if (p < 30) power_loss_f.BackColor = Color.Red;
			else
			{
				if (p < 50) power_loss_f.BackColor = Color.Yellow;
				else power_loss_f.BackColor = Color.Lime;
			}
			power_loss_f.Update();
			fault_rates[9] = p;
			error_names[9] = "CHECK for POWER LOSS.";
		}

		private void shut_down_f_refresh()
		{
			double p = calcShutDownCircuitOn();
			shut_down_f.Text = p.ToString("N2") + "% normal";
			if (p < 30) shut_down_f.BackColor = Color.Red;
			else
			{
				if (p < 50) shut_down_f.BackColor = Color.Yellow;
				else shut_down_f.BackColor = Color.Lime;
			}
			shut_down_f.Update();
			fault_rates[10] = p;
			error_names[10] = "CHECK SHUT-DOWN CIRCUIT.";
		}

		List<double> LV_voltage_readings = new List<double>();
		List<double> HV_curr_readings = new List<double>();
		List<double> engine_rpm_readings = new List<double>();
		List<double> HV_temp_readings = new List<double>();
		List<double> water_temp_readings = new List<double>();
		List<double> water_press_readings = new List<double>();
		List<double> LV_curr_readings = new List<double>();

		private void LV_voltage_refresh(double reading)
        {
			state s = state.NORMAL;
			LV_voltage_readings.Add(reading);
			LV_bat_value.Text = reading.ToString("N2") + " V";
			LV_bat_value.Update();
			int num = 58;
			if (reading < 12.8 || reading > 17.8)
			{
				LV_bat_value_bar.BackColor = Color.Red;
			    s = state.FAULT;
			}
			else {
				num = (int)(58 * (reading - 12.8) / 5);
				if (reading < 13.3 || reading > 17.3)
				{
					LV_bat_value_bar.BackColor = Color.Yellow;
					s = state.EDGE;
				}
				else
				{
					LV_bat_value_bar.BackColor = Color.Lime;
					s = state.NORMAL;
				}
			}
			string text = "";
			for (int i = 1; i <= num; i++)
			{
				text = text + " ";
			}
			LV_bat_value_bar.Text = text;
			LV_bat_value_bar.Update();
			sensor_state[0] = s;

			LV_voltage_chart.Series.Clear();
			LV_voltage_chart.Series.Add("bla");
			LV_voltage_chart.Series["bla"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			int last = LV_voltage_readings.Count - 1;
			for (int i = -29; i < 1; i++)
			{
				LV_voltage_chart.Series["bla"].Points.AddXY(i, LV_voltage_readings[last + i]);
			}
			LV_voltage_chart.Update();
		}

		private void HV_curr_refresh(double reading)
		{
			state s = state.NORMAL;
			HV_curr_readings.Add(reading);
			HV_curr_value.Text = reading.ToString("N1") + " A";
			HV_curr_value.Update();
			int num = 58;
			if (reading < 130 || reading > 170)
			{
				HV_curr_value_bar.BackColor = Color.Red;
				s = state.FAULT;
			}
			else
			{
				num = (int)(58 * (reading - 130) / 40);
				if (reading < 135 || reading > 165)
				{
					HV_curr_value_bar.BackColor = Color.Yellow;
					s = state.EDGE;
				}
				else
				{
					HV_curr_value_bar.BackColor = Color.Lime;
					s = state.NORMAL;
				}
			}
			string text = "";
			for (int i = 1; i <= num; i++)
			{
				text = text + " ";
			}
			HV_curr_value_bar.Text = text;
			HV_curr_value_bar.Update();
			sensor_state[1] = s;

			HV_curr_chart.Series.Clear();
			HV_curr_chart.Series.Add("jes");
			HV_curr_chart.Series["jes"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			int last = HV_curr_readings.Count - 1;
			for (int i = -29; i < 1; i++)
			{
				HV_curr_chart.Series["jes"].Points.AddXY(i, HV_curr_readings[last + i]);
			}
			HV_curr_chart.Update();
		}

		private void engine_rpm_refresh(double reading)
		{
			reading = reading / 1000;
			state s = state.NORMAL;
			engine_rpm_readings.Add(reading);
			engine_speed_value.Text = reading.ToString("N1") + " krpm";
			HV_curr_value.Update();
			int num = 58;
			if (reading < 9 || reading > 19)
			{
				engine_speed_value_bar.BackColor = Color.Red;
				s = state.FAULT;
			}
			else
			{
				num = (int)(58 * (reading - 9) / 10);
				if (reading < 10 || reading > 18)
				{
					engine_speed_value_bar.BackColor = Color.Yellow;
					s = state.EDGE;
				}
				else
				{
					engine_speed_value_bar.BackColor = Color.Lime;
					s = state.NORMAL;
				}
			}
			string text = "";
			for (int i = 1; i <= num; i++)
			{
				text = text + " ";
			}
			engine_speed_value_bar.Text = text;
			engine_speed_value_bar.Update();
			sensor_state[2] = s;


			engine_speed_chart.Series.Clear();
			engine_speed_chart.Series.Add("jes");
			engine_speed_chart.Series["jes"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			int last = engine_rpm_readings.Count - 1;
			for (int i = -29; i < 1; i++)
			{
				engine_speed_chart.Series["jes"].Points.AddXY(i, engine_rpm_readings[last + i]);
			}
			engine_speed_chart.Update();
		}
		
		private void HV_temp_refresh(double reading)
		{
			state s = state.NORMAL;
			HV_temp_readings.Add(reading);
			HV_bat_temp_value.Text = reading.ToString("N1") + " C";
			HV_bat_temp_value.Update();
			int num = 58;
			if (reading < 30 || reading > 60)
			{
				HV_bat_temp_value_bar.BackColor = Color.Red;
				s = state.FAULT;
			}
			else
			{
				num = (int)(58 * (reading - 30) / 30);
				if (reading < 35 || reading > 55)
				{
					HV_bat_temp_value_bar.BackColor = Color.Yellow;
					s = state.EDGE;
				}
				else
				{
					HV_bat_temp_value_bar.BackColor = Color.Lime;
					s = state.NORMAL;
				}
			}
			string text = "";
			for (int i = 1; i <= num; i++)
			{
				text = text + " ";
			}
			HV_bat_temp_value_bar.Text = text;
			HV_bat_temp_value_bar.Update();
			sensor_state[3] = s;

			HV_temp_chart.Series.Clear();
			HV_temp_chart.Series.Add("bla");
			HV_temp_chart.Series["bla"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			int last = HV_temp_readings.Count - 1;
			for (int i = -29; i < 1; i++)
			{
				HV_temp_chart.Series["bla"].Points.AddXY(i, HV_temp_readings[last + i]);
			}
			HV_temp_chart.Update();
		}
		
		private void water_temp_refresh(double reading)
		{
			state s = state.NORMAL;
			water_temp_readings.Add(reading);
			water_temp_value.Text = reading.ToString("N1") + " C";
			water_temp_value.Update();
			int num = 58;
			if (reading < 25 || reading > 35)
			{
				water_temp_value_bar.BackColor = Color.Red;
				s = state.FAULT;
			}
			else
			{
				num = (int)(58 * (reading - 25) / 10);
				if (reading < 27 || reading > 33)
				{
					water_temp_value_bar.BackColor = Color.Yellow;
					s = state.EDGE;
				}
				else
				{
					water_temp_value_bar.BackColor = Color.Lime;
					s = state.NORMAL;
				}
			}
			string text = "";
			for (int i = 1; i <= num; i++)
			{
				text = text + " ";
			}
			water_temp_value_bar.Text = text;
			water_temp_value_bar.Update();
			sensor_state[4] = s;

			water_temp_chart.Series.Clear();
			water_temp_chart.Series.Add("bla");
			water_temp_chart.Series["bla"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			int last = water_temp_readings.Count - 1;
			for (int i = -29; i < 1; i++)
			{
				water_temp_chart.Series["bla"].Points.AddXY(i, water_temp_readings[last + i]);
			}
			water_temp_chart.Update();
		}
		
		private void water_press_refresh(double reading)
		{
			state s = state.NORMAL;
			water_press_readings.Add(reading);
			water_pres_value.Text = reading.ToString("N1") + " kPa";
			water_pres_value.Update();
			int num = 58;
			if (reading < 110 || reading > 150)
			{
				water_pres_value_bar.BackColor = Color.Red;
				s = state.FAULT;
			}
			else
			{
				num = (int)(58 * (reading - 110) / 40);
				if (reading < 115 || reading > 145)
				{
					water_pres_value_bar.BackColor = Color.Yellow;
					s = state.EDGE;
				}
				else
				{
					water_pres_value_bar.BackColor = Color.Lime;
					s = state.NORMAL;
				}
			}
			string text = "";
			for (int i = 1; i <= num; i++)
			{
				text = text + " ";
			}
			water_pres_value_bar.Text = text;
			water_pres_value_bar.Update();
			sensor_state[5] = s;

			water_press_chart.Series.Clear();
			water_press_chart.Series.Add("bla");
			water_press_chart.Series["bla"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			int last = water_press_readings.Count - 1;
			for (int i = -29; i < 1; i++)
			{
				water_press_chart.Series["bla"].Points.AddXY(i, water_press_readings[last + i]);
			}
			water_press_chart.Update();
		}
		
		private void LV_curr_refresh(double reading)
		{
			state s = state.NORMAL;
			LV_curr_readings.Add(reading);
			LV_curr_value.Text = reading.ToString("N1") + " A";
			LV_curr_value.Update();
			int num = 58;
			if (reading < 20 || reading > 28)
			{
				LV_curr_value_bar.BackColor = Color.Red;
				s = state.FAULT;
			}
			else
			{
				num = (int)(58 * (reading - 20) / 8);
				if (reading < 21 || reading > 27)
				{
					LV_curr_value_bar.BackColor = Color.Yellow;
					s = state.EDGE;
				}
				else
				{
					LV_curr_value_bar.BackColor = Color.Lime;
					s = state.NORMAL;
				}
			}
			string text = "";
			for (int i = 1; i <= num; i++)
			{
				text = text + " ";
			}
			LV_curr_value_bar.Text = text;
			LV_curr_value_bar.Update();
			sensor_state[6] = s;

			LV_curr_chart.Series.Clear();
			LV_curr_chart.Series.Add("bla");
			LV_curr_chart.Series["bla"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			int last = LV_curr_readings.Count - 1;
			for (int i = -29; i < 1; i++)
			{
				LV_curr_chart.Series["bla"].Points.AddXY(i, LV_curr_readings[last + i]);
			}
			LV_curr_chart.Update();
		}

		private void button1_Click_1(object sender, EventArgs e){
            BayesianNetwork.Visible = true;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BayesianNetwork.Visible = false;
            LV_voltage_chart.Visible = true;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = false;
        }

        private void HV_curr_button_Click(object sender, EventArgs e)
        {
            BayesianNetwork.Visible = false;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = true;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = false;
        }

        private void engine_speed_button_Click(object sender, EventArgs e){
			BayesianNetwork.Visible = false;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = true;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BayesianNetwork.Visible = false;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = true;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = false;
        }

        private void water_temp_button_Click(object sender, EventArgs e){
            BayesianNetwork.Visible = false;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = true;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = false;
        }

        private void water_press_button_Click(object sender, EventArgs e){
            BayesianNetwork.Visible = false;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = true;
            LV_curr_chart.Visible = false;
        }

        private void LV_curr_button_Click(object sender, EventArgs e){
            BayesianNetwork.Visible = false;
            LV_voltage_chart.Visible = false;
            HV_curr_chart.Visible = false;
            engine_speed_chart.Visible = false;
            HV_temp_chart.Visible = false;
            water_temp_chart.Visible = false;
            water_press_chart.Visible = false;
            LV_curr_chart.Visible = true;
        }

		private bool start_stop = false;

		private void button1_Click(object sender, EventArgs e) 
		{
			start_stop = !start_stop;
            if (start_stop)
            {
				Start.Text = "STOP";
				Start.BackColor = Color.Red;
				timer.Start();
            }
			else
            {
				Start.Text = "START";
				Start.BackColor = Color.Lime;
				timer.Stop();
			}
		}

		private enum state { NORMAL = 0, FAULT = 1, EDGE = 2 };

		private double denumerator = 0;
		// state	N	F
		private double[] BMS_fault = { 0.95, 0.05 };
		private double[] IMD_fault = { 0.96, 0.04 };
		private double[] engine_fault = { 0.98, 0.02 };
		private double[] invertor_fault = { 0.97, 0.03 };
		private double[] battery_low = { 0.89, 0.11 };
		private double[] pump_fault = { 0.91, 0.09 };
		private double[] ventilator_fault = { 0.91, 0.09 };
		private double[] cooling_system_fault = { 0.92, 0.08 };

		// BMS_F	N	N	F	F
		// IMD_F	N	F	N	F
		private double[] shut_down = { 0.99, 0.1, 0.15, 0.01 };

		// engine_f		N	N	N	N	F	F	F	F	
		// invertor_f	N	N	F	F	N	N	F	F
		// battery_f	N	F	N	F	N	F	N	F
		private double[] power_loss = { 0.99, 0.6, 0.2, 0.05, 0.2, 0.05, 0.05, 0.01 };

		// pump_f				N	N	N	N	F	F	F	F	
		// cooling_system_f		N	N	F	F	N	N	F	F
		// ventilator_f			N	F	N	F	N	F	N	F
		private double[] overheat = { 0.99, 0.15, 0.40, 0.15, 0.10, 0.05, 0.10, 0.01 };

		// shut down	N	F
		private double[,] LV_battery_voltage = { {0.80, 0.01}, 
												 {0.01, 0.90},
												 {0.19, 0.09} };

		// shut_down	N	N	F	F
		// power_loss	N	F	N	F
		private double[,] HV_current_sensor = { {0.90, 0.20, 0.15, 0.01},
												{0.01, 0.50, 0.65, 0.90},
												{0.09, 0.30, 0.20, 0.09} };

		// power_loss	N	F
		private double[,] engine_rotary_speed = { {0.90, 0.01}, 
												  {0.01 ,0.90}, 
												  {0.09, 0.09}};

		// shut_down	N	N	N	N	F	F	F	F	
		// power_loss	N	N	F	F	N	N	F	F
		// overheat		N	F	N	F	N	F	N	F
		private double[,] HV_battery_temperature = { {0.90, 0.10, 0.35, 0.09, 0.60, 0.05, 0.05, 0.01}, 
													 {0.01, 0.75, 0.50, 0.90, 0.10, 0.80, 0.80, 0.90}, 
													 {0.09, 0.15, 0.15, 0.01, 0.30, 0.15, 0.15, 0.09} };

		// power_loss	N	N	F	F
		// overheat		N	F	N	F
		private double[,] water_temperature_sensor_invertor_engine = { {0.90, 0.05, 0.60, 0.01},
																	   {0.01, 0.91, 0.22, 0.90},
																	   {0.09, 0.04, 0.18, 0.09}};

		// overheat		N	F
		private double[,] water_preasure_sensor = { {0.80, 0.05},
												    {0.05, 0.80},
												    {0.15, 0.15} };

		// overheat		N	F
		private double[,] current_sensor_LV = { {0.90, 0.05},  
												{0.02, 0.85},
												{0.08, 0.10} };

		// sensor_states		LV_bat_u, HV_curr, eng_speed, HV_bat_t, water_t, water_p, LV_curr
		private state[] sensor_state = { state.NORMAL, state.NORMAL, state.NORMAL, state.NORMAL, state.NORMAL, state.NORMAL, state.NORMAL };


		private double calcBMSf()
		{
			double aposterior_BMSf = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_BMSf = aposterior_BMSf + pdf(state.NORMAL, (state)a, (state)b, (state)c, (state)d,
														(state)e, (state)f, (state)g, (state)h, (state)i, (state)j, sensor_state[0],
														sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
														sensor_state[6]);
			return (aposterior_BMSf / denumerator * 100);
		}

		private double calcIMDf()
		{
			double aposterior_IMDf = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_IMDf = aposterior_IMDf + pdf((state)a, state.NORMAL, (state)b, (state)c, (state)d,
														(state)e, (state)f, (state)g, (state)h, (state)i, (state)j, sensor_state[0],
														sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
														sensor_state[6]);
			return (aposterior_IMDf / denumerator * 100);
		}

		private double calcEnginef()
		{
			double aposterior_engine_f = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_engine_f = aposterior_engine_f + pdf((state)a, (state)b, state.NORMAL, (state)c, (state)d,
														(state)e, (state)f, (state)g, (state)h, (state)i, (state)j, sensor_state[0],
														sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
														sensor_state[6]);
			return (aposterior_engine_f / denumerator * 100);
		}

		private double calcInvertorf()
		{
			double aposterior_invertor_f = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_invertor_f = aposterior_invertor_f + pdf((state)a, (state)b, (state)c, state.NORMAL,
														(state)d, (state)e, (state)f, (state)g, (state)h, (state)i, (state)j, sensor_state[0],
														sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
														sensor_state[6]);
			return (aposterior_invertor_f / denumerator * 100);
		}

		private double calcBatteryLowf()
		{
			double aposterior_battery_low_f = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_battery_low_f = aposterior_battery_low_f + pdf((state)a, (state)b, (state)c,
														(state)d, state.NORMAL, (state)e, (state)f, (state)g, (state)h, (state)i, (state)j,
														sensor_state[0], sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4],
														sensor_state[5], sensor_state[6]);
			return (aposterior_battery_low_f / denumerator * 100);
		}

		private double calcPumpf()
		{
			double aposterior_pump_f = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_pump_f = aposterior_pump_f + pdf((state)a, (state)b, (state)c,
														(state)d, (state)e, state.NORMAL, (state)f, (state)g, (state)h, (state)i, (state)j,
														sensor_state[0], sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4],
														sensor_state[5], sensor_state[6]);
			return (aposterior_pump_f / denumerator * 100);
		}

		private double calcVentilatorf()
		{
			double aposterior_ventilator_f = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_ventilator_f = aposterior_ventilator_f + pdf((state)a, (state)b, (state)c,
														(state)d, (state)e, (state)f, state.NORMAL, (state)g, (state)h, (state)i, (state)j,
														sensor_state[0], sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4],
														sensor_state[5], sensor_state[6]);
			return (aposterior_ventilator_f / denumerator * 100);
		}

		private double calcCoolingSystemf()
		{
			double aposterior_cooling_system_f = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													aposterior_cooling_system_f = aposterior_cooling_system_f + pdf((state)a, (state)b, (state)c,
														(state)d, (state)e, (state)f, (state)g, state.NORMAL, (state)h, (state)i, (state)j,
														sensor_state[0], sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4],
														sensor_state[5], sensor_state[6]);
			return (aposterior_cooling_system_f / denumerator * 100);
		}

		private double calcShutDownCircuitOn()
		{
			double shut_down_circuit_on = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													shut_down_circuit_on = shut_down_circuit_on + pdf((state)a, (state)b, (state)c, (state)d,
														(state)e, (state)f, (state)g, (state)h, state.NORMAL, (state)i, (state)j, sensor_state[0],
														sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
														sensor_state[6]);
			return (shut_down_circuit_on / denumerator * 100);
		}

		private double calcPowerLoss()
		{
			double power_loss = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													power_loss = power_loss + pdf((state)a, (state)b, (state)c, (state)d,
														(state)e, (state)f, (state)g, (state)h, (state)i, state.NORMAL, (state)j, sensor_state[0],
														sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
														sensor_state[6]);
			return (power_loss / denumerator * 100);
		}

		private double calcOverheat()
		{
			double overheat = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													overheat = overheat + pdf((state)a, (state)b, (state)c, (state)d,
														(state)e, (state)f, (state)g, (state)h, (state)i, (state)j, state.NORMAL, sensor_state[0],
														sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
														sensor_state[6]);
			return (overheat / denumerator * 100);
		}

		private double getShutDown(state BMS_f, state IMD_f, state s)
		{
			if (s == state.NORMAL) return shut_down[((int)BMS_f * 2 + (int)IMD_f * 1)];
			else return (1 - shut_down[((int)BMS_f * 2 + (int)IMD_f * 1)]);
		}

		private double getPowerLoss(state engine_f, state invertor_f, state battery_low_f, state s)
		{
			if (s == state.NORMAL) return power_loss[((int)engine_f * 4 + (int)invertor_f * 2 + (int)battery_low_f * 1)];
			else return (1 - power_loss[((int)engine_f * 4 + (int)invertor_f * 2 + (int)battery_low_f * 1)]);
		}

		private double getOverheat(state pump_f, state cooling_system_f, state ventilator_f, state s)
		{
			if (s == state.NORMAL) return overheat[((int)pump_f * 4 + (int)cooling_system_f * 2 + (int)ventilator_f * 1)];
			else return (1 - overheat[((int)pump_f * 4 + (int)cooling_system_f * 2 + (int)ventilator_f * 1)]);
		}

		private double getLVBatteryVoltage(state shut_down, state s)
		{
			return LV_battery_voltage[(int)s,(int)shut_down];
		}

		private double getHVCurrentSensor(state shut_down, state power_loss, state s)
		{
			return HV_current_sensor[(int)s,((int)shut_down * 2 + (int)power_loss)];
		}

		private double getEngineRotarySpeedSensor(state power_loss, state s)
		{
			return engine_rotary_speed[(int)s, (int)power_loss];
		}

		private double getHVBatteryTemperatureSensor(state shut_down, state power_loss, state overheat, state s)
		{
			return HV_battery_temperature[(int)s, ((int)shut_down * 4 + (int)power_loss * 2 + (int)overheat)];
		}

		private double getWaterTemperatureSensorInvertorEngine(state power_loss, state overheat, state s)
		{
			return water_temperature_sensor_invertor_engine[(int)s, (int)power_loss * 2 + (int)overheat];
		}

		private double getWaterPressureSensor(state overheat, state s)
		{
			return water_preasure_sensor[(int)s, (int)overheat];
		}

		private double getCurrentSensorLV(state overheat, state s)
		{
			return current_sensor_LV[(int)s, (int)overheat];
		}

		private double pdf(state BMS_s, state IMD_s, state engine_s, state invertor_s, state battery_low_s, state pump_s, state ventilator_s, state cooling_system_s,
			state shut_down_s, state power_loss_s, state overheat_s, state LV_battery_voltage_s, state HV_current_sensor_s, state engine_rotary_speed_s,
			state HV_battery_temperature_s, state water_temperature_sensor_invertor_engine_s, state water_pressure_sensor_s, state current_sensor_LV_s)
		{

			return BMS_fault[(int)BMS_s] * IMD_fault[(int)IMD_s] * engine_fault[(int)engine_s] * invertor_fault[(int)invertor_s] * battery_low[(int)battery_low_s] * pump_fault[(int)pump_s] *
				ventilator_fault[(int)ventilator_s] * cooling_system_fault[(int)cooling_system_s] * getShutDown(BMS_s, IMD_s, shut_down_s) *
				getPowerLoss(engine_s, invertor_s, battery_low_s, power_loss_s) * getOverheat(pump_s, cooling_system_s, ventilator_s, overheat_s) *
				getLVBatteryVoltage(shut_down_s, LV_battery_voltage_s) * getHVCurrentSensor(shut_down_s, power_loss_s, HV_current_sensor_s) *
				getEngineRotarySpeedSensor(power_loss_s, engine_rotary_speed_s) * getHVBatteryTemperatureSensor(shut_down_s, power_loss_s, overheat_s, HV_battery_temperature_s) *
				getWaterTemperatureSensorInvertorEngine(power_loss_s, overheat_s, water_temperature_sensor_invertor_engine_s) *
				getWaterPressureSensor(overheat_s, water_pressure_sensor_s) * getCurrentSensorLV(overheat_s, current_sensor_LV_s);

		}

		private double calcDenumertator()
		{
			double denumerator = 0;
			for (int a = 0; a < 2; a++)
				for (int b = 0; b < 2; b++)
					for (int c = 0; c < 2; c++)
						for (int d = 0; d < 2; d++)
							for (int e = 0; e < 2; e++)
								for (int f = 0; f < 2; f++)
									for (int g = 0; g < 2; g++)
										for (int h = 0; h < 2; h++)
											for (int i = 0; i < 2; i++)
												for (int j = 0; j < 2; j++)
													for (int k = 0; k < 2; k++)
														denumerator = denumerator + pdf((state)a, (state)b, (state)c, (state)d, (state)e,
															(state)f, (state)g, (state)h, (state)i, (state)j, (state)k, sensor_state[0],
															sensor_state[1], sensor_state[2], sensor_state[3], sensor_state[4], sensor_state[5],
															sensor_state[6]);
			return denumerator;
		}

		private void HV_curr_chart_Click(object sender, EventArgs e){ }

        private void engine_speed_chart_Click(object sender, EventArgs e){ }

        private void Bashboard_Load(object sender, EventArgs e) { }

        private void BayesianNetwork_Paint(object sender, PaintEventArgs e) { }

        private void label1_Click(object sender, EventArgs e) { }

        private void BN_Click(object sender, EventArgs e) { }

        private void label1_Click_1(object sender, EventArgs e) { }

        private void label3_Click(object sender, EventArgs e) { }

        private void label10_Click(object sender, EventArgs e) { }

        private void label9_Click(object sender, EventArgs e) { }

        private void LV_voltage_chart_Click(object sender, EventArgs e) { }

        private void label8_Click(object sender, EventArgs e) { }

        private void label13_Click(object sender, EventArgs e) { }

        private void label4_Click(object sender, EventArgs e) { }

        private void label7_Click(object sender, EventArgs e) { }

        private void panel1_Paint(object sender, PaintEventArgs e){ }

        private void label3_Click_1(object sender, EventArgs e){ }

        private void baterry_low_Click(object sender, EventArgs e){ }
    }
}
